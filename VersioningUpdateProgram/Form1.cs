﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using VaultClientIntegrationLib;
using VaultClientOperationsLib;
using VaultLib;



/*
 *DESCRIPTION: Project creates a windows application form 
 *  and handles all code through event handlers based on button
 *  cloicks etc.
 */
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        /* 
         *FUNCTION NAME: Form Start
         *DESCRIPTION: Initializiation code to display form
         */
        public Form1()
        {
            InitializeComponent();
        }


        /*
         * FUNCTION NAME: BUTTON CLICK
         * DESCRIPTION: All code relating to reading version numbers
         *  and creating version number macro is contained within 
         */
        public void button1_Click(object sender, EventArgs e)
        {
            this.statusT.ScrollBars = ScrollBars.Vertical;
            this.statusT.Text = "Initial Setup";
            // DECLARE ALL VARIABLES
            string[] sFilepaths;
            string[] sFilepaths2;
            string sTemp;
            string[] sTempArray;
            string[] sTempArray2;
            string[] sTempArray3;
            int macrosToFind = 18;
            int[] iTempArray = new int[macrosToFind];
            int iTemp = 0;
            long iFPVersion = 0;
            int iCLEVersion = 0;
            int iCounter = 0;
            string sVisionVersion = "";
            int i8281Version = 0;
            int i9012Version = 0;
            int i9013Version = 0;
            int i9020Version = 0;
            int i9021Version = 0;
            int i9022Version = 0;
            int i9023Version = 0;
            int i9024Version = 0;
            int i9025Version = 0;
            int i9026Version = 0;
            int i9027Version = 0;
            int i9028Version = 0;
            int i9029Version = 0;
            int i9030Version = 0;
            int i9044Version = 0;
            int i9045Version = 0;
            int i9046Version = 0;
            int i9047Version = 0;
            int iTempVersion;
            int iFPCharNum = 0;
            System.IO.StreamReader objReader;
            System.IO.StreamWriter objWriter;
            System.Text.StringBuilder sBuilder;
            bool bReadLine = false;
            string[] sXMLArray;
            List<String> list = new List<String>();

            //GET DATA FROM TEXT BOXES
            string folderPath = fPath.Text;
            folderPath = folderPath.Replace(@"\", @"\\");
            bool updated = updateLastMacro();
            //QUIT IF UPDATE FAILED
            if (updated == false) return;
            //Set first and last macro variables
            int firstMacro = Convert.ToInt32(this.firstMacroT.Text);
            int numVars = Convert.ToInt32(this.numVarsT.Text);
            long[] versions = new long[numVars];
            int verCounter = 0;
            string macroName = this.macroNameT.Text;
            // Vault settings
            string userName = this.userNameT.Text;
            string password = this.passwordT.Text;
            string vaultFolder = this.vaultFolderT.Text;
            vaultFolder = vaultFolder.Replace(@"\", @"\\");

            /*
             * CONNECT TO VAULT
             */
            this.statusT.Text = "STATUS: Logging in to Vault...";
            string url = "http://eivault.electroimpact.com/VaultService";
            string repository = "EI";
            string vaultName;

            // Set the login options and login/connect to a repository.
            ServerOperations.client.LoginOptions.URL = url;
            ServerOperations.client.LoginOptions.User = userName;
            ServerOperations.client.LoginOptions.Password = password;
            ServerOperations.client.LoginOptions.Repository = repository;
            try
            {
                ServerOperations.Login();
            }
            catch (Exception ex)
            {
                string error = "STATUS: Unable to connect to Vault.";
                error += "Check Username, password and internet connection and try again.\r\n";
                error += "Error Thrown: " + ex.ToString();
                this.statusT.Text = error;
                return;
            }
            ServerOperations.client.AutoCommit = true;
            this.statusT.Text = "STATUS: Successfully logged into vault, getting Fanuc Picture version #";
            try
            {
                VaultClientFolder folder = ServerOperations.ProcessCommandListFolder(vaultFolder, true);
                //FANUC PICTURE VERSION 
                iFPVersion = 0;
                foreach (VaultClientFile file in folder.Files)
                {
                    if (file.Name.Contains(".MEM"))
                    {
                        VaultFileProperties props = file.GetFileProperties();
                        iFPVersion = props.LatestVersion;
                        vaultName = props.LabeledOrigin;
                    }
                }
                versions[verCounter] = iFPVersion; verCounter++;
            }
            catch (Exception ex)
            {
                string error = "STATUS: Logged In! Unable to get list folder from vault";
                error += "Error Thrown: " + ex.ToString();
                this.statusT.Text = error;
                return;
            }





            //CLE VERSION
            this.statusT.Text = "STATUS: Getting CLE version #";
            iTempVersion = 0;
            objReader = new System.IO.StreamReader(folderPath + "\\CLE\\src\\Version.C");
            sTemp = objReader.ReadToEnd();
            sTempArray = Regex.Split(sTemp, "FTCv");
            for (int i = 0; i <= (sTempArray.Count() - 1); i++)
            {
                if ((i % 2 == 0) && (i != 0))
                {
                    sTempArray2 = sTempArray[i].Split('"');
                    sTemp = sTempArray2[0];
                    sTemp = sTemp.TrimStart('.');
                    iTempVersion = Int32.Parse(sTemp);
                    if (iTempVersion > iCLEVersion)
                    {
                        iCLEVersion = iTempVersion;
                        //WRITE VERSION TO ARRAY
                        versions[verCounter] = iCLEVersion; verCounter++;
                    }
                }
            }
            label2.Text = "CLE: " + iTempVersion.ToString();


            //VISION VERSION
            this.statusT.Text = "STATUS: Getting Vision version #";
            iTempVersion = 0;
            objReader = new System.IO.StreamReader(folderPath + "\\EI Vision\\Camera Display\\AssemblyInfo.cs");
            sTemp = objReader.ReadToEnd();
            sTempArray = Regex.Split(sTemp, "AssemblyVersion");
            for (int i = 0; i <= (sTempArray.Count() - 1); i++)
            {
                if (i % 2 != 0)
                {
                    sTempArray2 = sTempArray[i].Split('"');
                    sTempArray2 = sTempArray2[1].Split('"');
                    sVisionVersion = sTempArray2[0];

                }
            }
            label3.Text = "Vision: " + sVisionVersion;


            //MACRO VERSIONS
            this.statusT.Text = "STATUS: Getting Macros version #s";
            iTempVersion = 0;
            String mFileName;
            String[] macroNamesInArray = new string[100];
            int macrosFound = 0;
            sFilepaths2 = Directory.GetFiles(folderPath + "\\Macros");
            int numMacrosToCheck = Math.Min(sFilepaths2.Count(), macrosToFind);
            for (int i = 0; i < (numMacrosToCheck); i++)
            {
                mFileName = sFilepaths2[i].ToUpper();
                if (mFileName.Contains(".MCR") || mFileName.Contains(".TXT"))
                {
                    objReader = new System.IO.StreamReader(sFilepaths2[i]);
                    sTemp = objReader.ReadToEnd();
                    sTempArray = Regex.Split(sTemp, "Revision:");
                    sTempArray = sTempArray[1].Split(' ');
                    sTemp = sTempArray[1];
                    iTempArray[macrosFound] = Int32.Parse(sTemp);
                    macroNamesInArray[macrosFound] = mFileName;
                    //WRITE VERSION TO ARRAY
                    versions[verCounter] = iTempArray[macrosFound]; verCounter++;
                    macrosFound++;
                }
            }
            //GET VERSION NUMBER
            i8281Version = iTempArray[0];
            i9012Version = iTempArray[1];
            i9013Version = iTempArray[2];
            i9020Version = iTempArray[3];
            i9021Version = iTempArray[4];
            i9022Version = iTempArray[5];
            i9023Version = iTempArray[6];
            i9024Version = iTempArray[7];
            i9025Version = iTempArray[8];
            i9026Version = iTempArray[9];
            i9027Version = iTempArray[10];
            i9028Version = iTempArray[11];
            i9029Version = iTempArray[12];
            i9030Version = iTempArray[13];
            i9044Version = iTempArray[14];
            i9045Version = iTempArray[15];
            i9046Version = iTempArray[16];
            i9047Version = iTempArray[17];
            //WRITE VERSION NUMBER TO LABEL
            label4.Text = "8281: " + i8281Version.ToString();
            label5.Text = "9012: " + i9012Version.ToString();
            label6.Text = "9013: " + i9013Version.ToString();
            label7.Text = "9020: " + i9020Version.ToString();
            label8.Text = "9021: " + i9021Version.ToString();
            label9.Text = "9022: " + i9022Version.ToString();
            label10.Text = "9023: " + i9023Version.ToString();
            label11.Text = "9024: " + i9024Version.ToString();
            label21.Text = "9025: " + i9025Version.ToString();
            label12.Text = "9026: " + i9026Version.ToString();
            label13.Text = "9027: " + i9027Version.ToString();
            label14.Text = "9028: " + i9028Version.ToString();
            label15.Text = "9029: " + i9029Version.ToString();
            label16.Text = "9030: " + i9030Version.ToString();
            label17.Text = "9044: " + i9044Version.ToString();
            label18.Text = "9045: " + i9045Version.ToString();
            label19.Text = "9046: " + i9046Version.ToString();
            label20.Text = "9047: " + i9047Version.ToString();


            //Loop through all version strings and write to file
            this.statusT.Text = "STATUS: Writing versions to macro program file";
            String macroProgramString = "%<" + macroName + ">\n";
            int curMacroNum;
            for (int j = 0; j < numVars; j++)
            {
                curMacroNum = firstMacro + j;
                macroProgramString += "#" + curMacroNum.ToString() + "=0" + versions[j] + "\n";
            }
            macroProgramString += "M02\n%";
            string macroFilePath = folderPath + @"\\" + macroName;
            File.WriteAllText(macroFilePath, macroProgramString);
            this.statusT.Text = "STATUS: Completed!\r\nOpen macro file to confirm correct data output";
            return;
        }
         


        /*
         *  FUNCTION: BROWSE BUTTON
         *  DESCRIPTION: Opens generic folder browser
         */
        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.fPath.Text = folderBrowserDialog1.SelectedPath;
            }       
        }


        //Allow only numbers to be entered into form
        private void firstMacroT_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void numVarsT_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        //Check if macro variable addresses text is changed
        private void numVarsT_TextChanged(object sender, EventArgs e)
        {
            updateLastMacro();
        }
        private void firstMacroT_TextChanged(object sender, EventArgs e)
        {
            updateLastMacro();
        }

        public bool updateLastMacro()
        {
            //STRINGS FROM INPUT BOXES
            string firstMacroString = this.firstMacroT.Text;
            string numVarsString = this.numVarsT.Text;
            int firstMacro;
            int numVars;
            // Check to make sure macro variable boxes are integers
            if (int.TryParse(firstMacroString, out firstMacro) == false)
            {
                MessageBox.Show("First Macro number must be an integer");
                return false;
            }
            if (int.TryParse(numVarsString, out numVars) == false)
            {
                MessageBox.Show("# of variables must be an integer");
                return false;
            }
            //Output last macro to textbox
            int lastMacro = firstMacro + numVars;
            this.lastMacroT.Text = lastMacro.ToString();
            return true;
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            string folderPath = fPath.Text;
            folderPath = folderPath.Replace(@"\", @"\\"); 
            string macroName = this.macroNameT.Text;
            string macroFilePath = folderPath + @"\\" + macroName;
            System.Diagnostics.Process.Start(macroFilePath);
        }

        private void vaultFiles()
        {
            string url = "http://eivault.electroimpact.com/VaultService";
            string username = "chrislo";
            string password = "Cncgetfancy#1";
            string repository = "EI";
            


            // Set the login options and login/connect to a repository.
            ServerOperations.client.LoginOptions.URL = url;
            ServerOperations.client.LoginOptions.User = username;
            ServerOperations.client.LoginOptions.Password = password;
            ServerOperations.client.LoginOptions.Repository = repository;
            ServerOperations.Login();
            ServerOperations.client.AutoCommit = true;
            MessageBox.Show("Successfully Logged Into Vault");

            // add some files to use with wildcard tasks
            string folderPath = "$/3832 787 47-48 Flex Track/Fanuc Picture";
            folderPath = folderPath.Replace(@"\", @"\\"); 
            string fanucPictureName = "FPF0FPDT.MEM";
            string fanucPicturePath = folderPath + @"\\" + fanucPictureName;
            string vaultName;
            long latestVersion; long version;
            VaultClientFolder folder = ServerOperations.ProcessCommandListFolder(folderPath, true);
            foreach (VaultClientFile file in folder.Files)
            {
                version = file.Version;
                VaultFileProperties props = file.GetFileProperties();
                latestVersion = props.LatestVersion;
                vaultName = props.LabeledOrigin;
                MessageBox.Show(vaultName + "  Version:" + latestVersion);
                System.Threading.Thread.Sleep(1000);
            }

        }

        private void vault_Click(object sender, EventArgs e)
        {
            vaultFiles();
        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
